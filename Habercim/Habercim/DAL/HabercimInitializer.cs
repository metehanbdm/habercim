﻿using Habercim.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Habercim.DAL
{
    public class HabercimInitializer : DropCreateDatabaseIfModelChanges<HabercimDbContext>
    {
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();
        HabercimDbContext db = new HabercimDbContext();

        protected override void Seed(HabercimDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(applicationDbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            Kullanici adminkullanici = new Kullanici
            {
                KullaniciId = 1,
                AdSoyad = "Administrator"
            };
            Kullanici kullanici = new Kullanici
            {
                KullaniciId=2,
                AdSoyad = "Metehan Badem"
            };
           
            db.Kullanicilar.Add(adminkullanici);
            db.SaveChanges();
            db.Kullanicilar.Add(kullanici);
            db.SaveChanges();

            /* admin icin kullanici bilgisi */
            ApplicationUser admin = new ApplicationUser
            {
                UserName = "admin@habercim.com",
                Email = "admin@habercim.com",
                KullaniciTipId = 1 
            };

            ApplicationUser ornekKullanici = new ApplicationUser
            {
                UserName = "metehanbadem@gmail.com",
                Email = "metehanbadem@gmail.com",
                KullaniciTipId = 2 
            };



            applicationDbContext.Users.Add(admin);
            applicationDbContext.Users.Add(ornekKullanici);
            applicationDbContext.SaveChanges();

            userManager.AddPassword(admin.Id, "Admin123+");
            userManager.AddPassword(ornekKullanici.Id, "Metehan123+");


            var rolStore = new RoleStore<IdentityRole>(applicationDbContext);
            var roleManager = new RoleManager<IdentityRole>(rolStore);

            roleManager.Create(new IdentityRole("Admin"));
            roleManager.Create(new IdentityRole("Kullanici"));

            userManager.AddToRole(admin.Id, "Admin");
            userManager.AddToRole(ornekKullanici.Id, "Kullanici");

        }
    }
}