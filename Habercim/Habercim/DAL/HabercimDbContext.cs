﻿using Habercim.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Habercim.DAL
{
    public class HabercimDbContext : DbContext
    {
        public HabercimDbContext() : base("HabercimDbContext")
        {

        }
      
        public DbSet<Manset> Mansetler { get; set; }
        public DbSet<Begeni> Begeniler { get; set; }
        public DbSet<Yorum> Yorumlar { get; set; }
        public DbSet<Kullanici> Kullanicilar { get; set; }
        public DbSet<Haber> Haberler { get; set; }
        public DbSet<Tur> Turler { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}