﻿using Habercim.DAL;
using Habercim.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Habercim.Controllers
{
    public class MenuController : Controller
    {
        HabercimDbContext db = new HabercimDbContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();


        [ChildActionOnly]
        public ActionResult KategoriMenu()
        {
            var kategoriler = db.Turler.ToList();
            return View(kategoriler);
        }
        [ChildActionOnly]
        public ActionResult UstMenu()
        {
            if (Request.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                    Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Kullanicilar.Where(x => x.KullaniciId == kullanici.KullaniciTipId).First();

                ViewBag.AdSoyad = kullaniciBilgi.AdSoyad;
                return View();
            }
            return View();
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}