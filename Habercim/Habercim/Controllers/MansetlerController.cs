﻿using Habercim.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Habercim.Controllers
{
    public class MansetlerController : Controller
    {
        HabercimDbContext db = new HabercimDbContext();

        [ChildActionOnly]
        public ActionResult Manset()
        {
            var manset = db.Mansetler.ToList();
            return View(manset);

        }
    }
}