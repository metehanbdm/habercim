﻿using Habercim.DAL;
using Habercim.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Habercim.Controllers
{
    public class HomeController : Controller
    {
        HabercimDbContext db=new HabercimDbContext();
        ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        public ActionResult Index()
        {
            var haberler = db.Haberler.ToList();
            return View(haberler);
        }
        public ActionResult TureGore(int? id)
        {

            if (id == null || db.Turler.Find(id).TurAd == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Baslik = db.Turler.Find(id).TurAd;
                var haberler = db.Haberler.Where(x => x.TurId == id);
                return View(haberler);
            }
        }
        public ActionResult Haberler(int? id)
        {

            if (id == null || db.Haberler.Find(id) == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Baslik = db.Haberler.Find(id).Tur.TurAd;
                ViewBag.Haber = db.Haberler.Find(id);

                if (Request.IsAuthenticated)
                {
                    var kullanici = applicationDbContext.Users.
                        Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                    var kullaniciBilgi = db.Kullanicilar.Where(x => x.KullaniciId == kullanici.KullaniciTipId).First();
                    if (db.Begeniler.Where(x => x.HaberId == id && x.KullaniciId == kullanici.KullaniciTipId).Count() > 0)
                    {
                        ViewBag.Durum = "Begenildi";
                        return View(db.Yorumlar.Where(x=>x.HaberId==id));
                    }
                    ViewBag.Durum = "Begenilmedi";
                    return View(db.Yorumlar.Where(x => x.HaberId == id));
                }
                ViewBag.Durum = "Giriş Yapılmadı";
                return View(db.Yorumlar.Where(x => x.HaberId == id));
            }
        }
        public ActionResult Begendiklerim()
        {
            if (Request.IsAuthenticated)
            {
                var kullanici = applicationDbContext.Users.
                    Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
                var kullaniciBilgi = db.Kullanicilar.Where(x => x.KullaniciId == kullanici.KullaniciTipId).First();
                if (db.Begeniler.Where(x=>x.KullaniciId==kullaniciBilgi.KullaniciId).Count() > 0)
                {
                    ViewBag.Mesaj = "var";
                    return View(db.Begeniler.Where(x => x.KullaniciId == kullaniciBilgi.KullaniciId).ToList());
                }
                ViewBag.Mesaj = "Henüz Hiç Bir Haberi Beğenmediniz.";
                return View();
            }
            return RedirectToAction("Login","Account",null);
        }
        public ActionResult YorumYap(int id,string yorum)
        {
            var kullanici = applicationDbContext.Users.
                   Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Kullanicilar.Where(x => x.KullaniciId == kullanici.KullaniciTipId).First();

            Yorum yeni = new Yorum()
            {
                HaberId=id,
                Metin=yorum,
                Tarih=DateTime.Now,
                KullaniciId=kullaniciBilgi.KullaniciId
               
            };
            db.Yorumlar.Add(yeni);
            db.SaveChanges();
            return RedirectToAction("Haberler", new { id = id });
        }
        public ActionResult Begenme(int id)
        {
            var kullanici = applicationDbContext.Users.
                    Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Kullanicilar.Where(x => x.KullaniciId == kullanici.KullaniciTipId).First();
            Begeni begeni = new Begeni()
            {
                HaberId = db.Haberler.Find(id).HaberId,
                KullaniciId = kullaniciBilgi.KullaniciId,
                Tarih = DateTime.Now
            };
            db.Begeniler.Add(begeni);
            db.SaveChanges();
            return RedirectToAction("Haberler", new { id = id });
        }
        public ActionResult BegenmeVazgec(int id)
        {
            var kullanici = applicationDbContext.Users.
                    Where(x => x.Email == AuthenticationManager.User.Identity.Name).First();
            var kullaniciBilgi = db.Kullanicilar.Where(x => x.KullaniciId == kullanici.KullaniciTipId).First();
            var begeni=db.Begeniler.Where(x=> x.HaberId==id&&x.KullaniciId==kullaniciBilgi.KullaniciId).First();
            db.Begeniler.Remove(begeni);
            db.SaveChanges();
            return RedirectToAction("Haberler", new { id = id });
        }
        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

    }
}