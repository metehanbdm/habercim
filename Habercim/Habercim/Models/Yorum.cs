﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Habercim.Models
{
    public class Yorum
    {
        public int YorumId { get; set; }
        public DateTime Tarih { get; set; }
        public string Metin { get; set; }
        public int HaberId { get; set; }
        public int KullaniciId { get; set; }

        public virtual Haber Haber { get; set; }
        public virtual Kullanici Kullanici { get; set; }

    }
}