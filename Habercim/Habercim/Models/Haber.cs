﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Habercim.Models
{
    public class Haber
    {
        public int HaberId { get; set; }
        public string Resim { get; set; }
        public string Baslik { get; set; }
        public string Aciklama { get; set; }
        public string Metin { get; set; }
        public DateTime Tarih { get; set; }
        public int TurId { get; set; }
        public virtual Tur Tur { get; set; }
        public ICollection<Begeni> Begeniler { get; set; }
        public ICollection<Yorum> Yorumlar { get; set; }


    }
}