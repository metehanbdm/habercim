﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Habercim.Models.Helpers
{
    public class HaberEkle
    {
        public int HaberId { get; set; }
        public string Baslik { get; set; }
        public string Aciklama { get; set; }
        public string Metin { get; set; }
        public int TurId { get; set; }
        [Display(Name = "Resim")]
        public string ResimUrl { get; set; }
        [Display(Name = "Resim")]
        public HttpPostedFileBase Resim { get; set; }
    }
}