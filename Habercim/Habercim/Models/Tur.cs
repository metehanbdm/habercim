﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Habercim.Models
{
    public class Tur
    {
        public int TurId { get; set; }
        public string TurAd { get; set; }
        public ICollection<Haber> Haberler { get; set; }

    }
}