﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Habercim.Models
{
    public class Manset
    {
        public int MansetId { get; set; }
        public int HaberId { get; set; }
        public virtual Haber Haber { get; set; }
    }
}