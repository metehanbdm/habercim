﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Habercim.Models
{
    public class Kullanici
    {
        public int KullaniciId { get; set; }
        public string AdSoyad { get; set; }
        public ICollection<Yorum> Yorumlar { get; set; }
        public ICollection<Begeni> Begeniler { get; set; }


    }
}