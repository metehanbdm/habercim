﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Habercim.Models
{
    public class Begeni
    {
        public int BegeniId { get; set; }
        public DateTime Tarih { get; set; }
        public int KullaniciId { get; set; }
        public int HaberId { get; set; }
        public virtual Kullanici Kullanici { get; set; }
        public virtual Haber Haber { get; set; }

    }
}