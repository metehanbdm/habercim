﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Habercim.DAL;
using Habercim.Models;

namespace Habercim.Areas.Admin.Controllers
{
    public class KullaniciController : GirisController
    {
        private HabercimDbContext db = new HabercimDbContext();

        // GET: Admin/Kullanici
        public ActionResult Index()
        {
            return View(db.Kullanicilar.ToList());
        }
    }
}
