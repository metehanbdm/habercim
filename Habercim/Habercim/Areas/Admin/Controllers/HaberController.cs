﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Habercim.DAL;
using Habercim.Models;
using Habercim.Models.Helpers;
using System.IO;

namespace Habercim.Areas.Admin.Controllers
{
    public class HaberController : GirisController
    {
        private HabercimDbContext db = new HabercimDbContext();

        // GET: Admin/Haber
        public ActionResult Index()
        {
            var haberler = db.Haberler.Include(h => h.Tur);
            return View(haberler.ToList());
        }

        // GET: Admin/Haber/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Haberler.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }

        const string imageFolderPath = "/Content/images/haberler/";
        // GET: Admin/Haber/Create
        public ActionResult Create()
        {
            ViewBag.TurId = new SelectList(db.Turler, "TurId", "TurAd");
            return View();
        }

        // POST: Admin/Haber/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HaberEkle eklenecekHaber)
        {
            if (ModelState.IsValid)
            {

                string fileName = string.Empty;

                if (eklenecekHaber.Resim != null && eklenecekHaber.Resim.ContentLength > 0)
                {
                    fileName = eklenecekHaber.Resim.FileName;
                    var path = Path.Combine(Server.MapPath(imageFolderPath), fileName);
                    /* eger varsa sil */
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    eklenecekHaber.Resim.SaveAs(path);
                }
                Haber haber = new Haber();
                haber.Aciklama = eklenecekHaber.Aciklama;
                haber.Baslik = eklenecekHaber.Baslik;
                haber.Metin = eklenecekHaber.Metin;
                haber.Tarih = DateTime.Now;
                haber.TurId = eklenecekHaber.TurId;
                haber.Resim= imageFolderPath + fileName;
                db.Haberler.Add(haber);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TurId = new SelectList(db.Turler, "TurId", "TurAd", eklenecekHaber.TurId);
            return View(eklenecekHaber);
        }

        // GET: Admin/Haber/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Haberler.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            HaberEkle degisecekHaber = new HaberEkle();
            degisecekHaber.HaberId = haber.HaberId;
            degisecekHaber.Aciklama = haber.Aciklama;
            degisecekHaber.Baslik = haber.Baslik;
            degisecekHaber.Metin = haber.Metin;
            degisecekHaber.ResimUrl = haber.Resim;
            degisecekHaber.TurId = haber.TurId;
            ViewBag.TurId = new SelectList(db.Turler, "TurId", "TurAd", haber.TurId);
            return View(degisecekHaber);
        }

        // POST: Admin/Haber/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HaberEkle degisHaber)
        {
            if (ModelState.IsValid)
            {
                string fileName = string.Empty;

                if (degisHaber.Resim != null && degisHaber.Resim.ContentLength > 0)
                {
                    fileName = degisHaber.Resim.FileName;
                    var path = Path.Combine(Server.MapPath(imageFolderPath), fileName);
                    /* eger varsa sil */
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    degisHaber.Resim.SaveAs(path);
                }
                var haber = db.Haberler.Find(degisHaber.HaberId);
                haber.Aciklama = degisHaber.Aciklama;
                haber.Baslik = degisHaber.Baslik;
                haber.Metin = degisHaber.Metin;
                haber.Tarih = DateTime.Now;
                haber.TurId = degisHaber.TurId;
                if (degisHaber.Resim != null)
                {
                    haber.Resim = imageFolderPath + fileName;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TurId = new SelectList(db.Turler, "TurId", "TurAd", degisHaber.TurId);
            return View(degisHaber);
        }

        // GET: Admin/Haber/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Haberler.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }

        // POST: Admin/Haber/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Haber haber = db.Haberler.Find(id);
            db.Haberler.Remove(haber);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
