﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Habercim.DAL;
using Habercim.Models;

namespace Habercim.Areas.Admin.Controllers
{
    public class MansetController : GirisController
    {
        private HabercimDbContext db = new HabercimDbContext();

        // GET: Admin/Manset
        public ActionResult Index()
        {
            var mansetler = db.Mansetler.Include(m => m.Haber);
            return View(mansetler.ToList());
        }

       
        // GET: Admin/Manset/Create
        public ActionResult Create()
        {
            ViewBag.HaberId = new SelectList(db.Haberler, "HaberId", "Resim");
            return View();
        }

        // POST: Admin/Manset/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MansetId,HaberId")] Manset manset)
        {
            if (ModelState.IsValid)
            {
                db.Mansetler.Add(manset);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.HaberId = new SelectList(db.Haberler, "HaberId", "Resim", manset.HaberId);
            return View(manset);
        }

        // GET: Admin/Manset/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manset manset = db.Mansetler.Find(id);
            if (manset == null)
            {
                return HttpNotFound();
            }
            return View(manset);
        }

        // POST: Admin/Manset/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Manset manset = db.Mansetler.Find(id);
            db.Mansetler.Remove(manset);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
