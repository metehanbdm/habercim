﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Habercim.Startup))]
namespace Habercim
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
